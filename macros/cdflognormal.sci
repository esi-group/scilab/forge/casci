//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdflognormal(X,a,b,c)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>4)
  error("incorrect number of arguments")
end
if ~exists("a","local")
  a=0
end
if ~exists("b","local")
  b=1
end
if ~exists("c","local")
  c=0
end
if b<=0
  error("argument ''b'' must be > 0")
end
Y=zeros(X)
i=(X>c)
if or(i)
  Y(i)=cdfnormal(a+b*log(X(i)-c))
end
endfunction