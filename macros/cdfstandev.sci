//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfstandev(X,n,sigma)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if (n<2)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 2")
end
if argin==2
  sigma=1
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
X=X/sigma
Y=zeros(X)
i=(X>0)
if or(i)
  Y(i)=cdfgamma(X(i).^2,(n-1)/2,2/(n-1))
end
endfunction