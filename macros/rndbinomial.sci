//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=rndbinomial(sz,n,p)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin~=3)
  error("incorrect number of arguments")
end
select size(sz,"*")
  case 1
    ro=sz(1)
    co=1
  case 2
    ro=sz(1)
    co=sz(2)
  else
    error("argument ''sz'' must contain a valid vector/matrix size")
end
if (ro<=0)|(ro~=floor(ro))|(co<=0)|(co~=floor(co))
  error("argument ''sz'' must contain a valid vector/matrix size")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if (p<0)|(p>1)
  error("argument ''p'' must be in [0,1]")
end
X=grand(ro,co,"bin",n,p)
endfunction