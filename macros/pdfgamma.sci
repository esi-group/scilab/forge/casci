//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfgamma(X,a,b,c,d)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>5)
  error("incorrect number of arguments")
end
if a<=0
  error("argument ''a'' must be > 0")
end
if ~exists("b","local")
  b=1
end
if ~exists("c","local")
  c=0
end
if ~exists("d","local")
  d=1
end
if b<=0
  error("argument ''b'' must be > 0")
end
if d==0
  error("argument ''d'' must be ~= 0")
end
Y=zeros(X)
i=(X>c)
if or(i)
  Zi=(X(i)-c)/b
  Y(i)=abs(d)*exp((a*d-1)*log(Zi)-Zi.^d-gammaln(a))/b
end
endfunction