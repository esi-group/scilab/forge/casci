//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdffoldednormal(X,mu,sigma,c)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>4)
  error("incorrect number of arguments")
end
if ~exists("mu","local")
  mu=0
end
if ~exists("sigma","local")
  sigma=1
end
if ~exists("c","local")
  c=0
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
Y=zeros(X)
i=(X>c)
if or(i)
  Xi=X(i)
  Y(i)=pdfnormal(Xi-c,mu,sigma)+pdfnormal(c-Xi,mu,sigma)
end
endfunction