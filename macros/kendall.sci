//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=kendall(X,Y)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
X=X(:)
Y=Y(:)
n=length(X)
ny=length(Y)
if n~=ny
  error("argument ''X'' and argument ''Y'' have incompatible sizes")
end
[u,ix]=sort(-X)
[u,iy]=sort(-Y)
[u,jy]=sort(-iy)
ry=jy(ix)
s=0
for i=1:n-1
  s=s+size(find(ry(i)<ry(i+1:n)),"*")-size(find(ry(i)>ry(i+1:n)),"*")
end
sd=sqrt(2*(2*n+5)/(9*n*(n-1)))
tau=2*s/(n*(n-1))
pv=2*cdfnormal(-abs(tau/sd))
endfunction