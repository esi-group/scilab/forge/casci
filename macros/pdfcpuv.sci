//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfcpuv(X,u,v,mut,sit,n)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=6
  error("incorrect number of arguments")
end
if u<0
  error("argument ''u'' must be >=0")
end
if v<0
  error("argument ''v'' must be >=0")
end
if (u==0)&(v==0)
  error("argument ''u'' and ''v'' cannot be = 0 simultaneously")
end
if sit<=0
  error("argument ''sit'' must be > 0")
end
if (n<=1)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 2")
end
del=1/sit
gam=abs(mut)/sit
D=del*sqrt(n)
g=gam*sqrt(n)
Y=X
k=size(Y,"*")
if u==0
  for i=1:k
    if X(i)<=0
      Y(i)=0
    else
      [t,tt]=quadlegendre(127,-D/(3*X(i)*sqrt(v)),D/(3*X(i)*sqrt(v)));
      Y(i)=sum(tt.*((2^(1-n/2)/(gamma((n-1)/2)))*(D^2/(9*X(i)^3*sqrt(%pi)))*..
            exp(-D^2/(18*X(i)^2)).*((D/(3*X(i)))^2-v*t^2)^((n-3)/2).*..
            exp(-((t-g)^2-v*t^2)/2)))
    end
  end
elseif v==0
  for i=1:k
    if X(i)<=0
      [t,tt]=quadlaguerre(127)
      Y(i)=sum(tt.*(2^(1-n/2)*3*t^(n-1)/(gamma((n-1)/2)*u*sqrt(%pi)).*..
            exp(-t^2/2).*(exp(-((D-3*X(i)*t)/u-g)^2/2)+..
            exp(-((D-3*X(i)*t)/u+g)^2/2))))
    else
      [t,tt]=quadlegendre(127,0,D/(3*X(i)))
      Y(i)=sum(tt.*(2^(1-n/2)*3*t^(n-1)/(gamma((n-1)/2)*u*sqrt(%pi)).*..
            exp(-t^2/2).*(exp(-((D-3*X(i)*t)/u-g)^2/2)+..
            exp(-((D-3*X(i)*t)/u+g)^2/2))))
    end
  end
else
  for i=1:k
    if X(i)<=-u/(3*sqrt(v))
      Y(i)=0
    elseif X(i)>0
      [t,tt]=quadlegendre(127,D*sqrt(v)/(u+3*X(i)*sqrt(v)),D/(3*X(i)))
      Y(i)=sum(tt.*((2^(1-n/2)/(gamma((n-1)/2))*..
           (3*t^2/(u*sqrt(%pi))).*((t^2-v*(3*X(i)*t-D)^2/u^2)^((n-3)/2)).*..
           exp(-(t^2-v*(3*X(i)*t-D)^2/u^2)/2).*(exp(-((D-3*X(i)*t)/u-g)^2/2)+..
           exp(-((D-3*X(i)*t)/u+g)^2/2)))))
    else
      [t,tt]=quadlaguerre(127,D*sqrt(v)/(u+3*X(i)*sqrt(v)))
      Y(i)=sum(tt.*((2^(1-n/2)/(gamma((n-1)/2))*..
           (3*t^2/(u*sqrt(%pi))).*((t^2-v*(3*X(i)*t-D)^2/u^2)^((n-3)/2)).*..
           exp(-(t^2-v*(3*X(i)*t-D)^2/u^2)/2).*(exp(-((D-3*X(i)*t)/u-g)^2/2)+..
           exp(-((D-3*X(i)*t)/u+g)^2/2)))))
    end
  end
end
Y(find(isnan(Y)))=0
endfunction