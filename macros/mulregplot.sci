//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function mulregplot(res)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
mnp=res(1)
n=mnp(2)
ye=res(3)
e=res(4)
y=ye+e
//
xset("window",0);xbasc(0)
plot2d([1:n;1:n;1:n;1:n]',[y,ye,y,ye],[1,5,-5,-4])
xselect()
endfunction