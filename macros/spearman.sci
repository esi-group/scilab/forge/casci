//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=spearman(X,Y)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
X=X(:)
Y=Y(:)
n=length(X)
ny=length(Y)
if n~=ny
  error("argument ''X'' and argument ''Y'' have incompatible sizes")
end
[u,ix]=sort(-X)
[u,jx]=sort(-ix)
[u,iy]=sort(-Y)
[u,jy]=sort(-iy)
r=sum((jx-jy).^2)
rs=1-6*r/(n*(n-1)*(n+1))
mu2=1/(n-1)
g2=3*(n-1)*(1+12*(n-2)*(n-3)/(25*n*(n-1)^2))/(n+1)-3
b=sqrt(2*(g2-2-sqrt(4-7*g2))/g2)/2
d=sqrt(768*mu2*b^6/(48*b^4-24*b^2+17))
pv=2*cdfjohnson(-abs(rs),"B",0,b,-d/2,d)
endfunction