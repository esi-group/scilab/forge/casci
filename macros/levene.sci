//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=levene(varargin)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin<2
  error("incorrect number of arguments")
end
k=argin
for i=1:k
  X=varargin(i)
  n(i)=size(X,"*")
  Z=abs(X-median(X))
  zb(i)=mean(Z)
  u(i)=sum((Z-zb(i)).^2)
end
nn=sum(n)
zbb=sum(n.*zb)/nn
w=((nn-k)*sum(n.*(zb-zbb).^2))/((k-1)*sum(u))
pv=1-cdffisher(w,k-1,nn-k)
endfunction