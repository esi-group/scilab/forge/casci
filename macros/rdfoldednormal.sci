//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function x=rdfoldednormal(sz,mu,sigma,c)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>4)
  error("incorrect number of arguments")
end
select size(sz,"*")
  case 1
    row=sz(1)
    col=1
  case 2
    row=sz(1)
    col=sz(2)
  else
    error("argument ''sz'' must contain a valid vector/matrix size")
end
if (row<=0)|(row~=floor(row))|(col<=0)|(col~=floor(col))
  error("argument ''sz'' must contain a valid vector/matrix size")
end
if ~exists("mu","local")
  mu=0
end
if ~exists("sigma","local")
  sigma=1
end
if ~exists("c","local")
  c=0
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
x=abs(rdnormal(sz,mu,sigma))+c
endfunction