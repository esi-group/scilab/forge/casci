//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function y=cdfwilcoxon2(x,m,n)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
if (m<=0)|(m~=floor(m))
  error("argument ''m'' must be an integer >= 1")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
mi=m*(m+1)/2
ma=mi+m*n
m1=m*(m+n+1)/2     
mu2=m*n*(m+n+1)/12
g2=(5*(m^2*n+m*n^2)-2*(m^2+n^2)+3*m*n-2*(m+n))*3/(5*m*n*(m+n+1))-3
b=sqrt(2*(g2-2-sqrt(4-7*g2))/g2)/2
d=sqrt(mu2/(1/(16*b^2)-1/(32*b^4)+17/(768*b^6)))
y=zeros(x)
i=find((x>=mi)&(x<ma))
if i~=[]
  y(i)=cdfjohnson(floor(x(i))-m1+0.5,"B",0,b,-d/2,d)
end
i=find(x>=ma)
y(i)=1
endfunction
//-----------------------------------------------------------------------------
function pv=wilcoxon2(X,Y,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if argin==2
  t="~"
end
m=size(X,"*")
n=size(Y,"*")
Z=[X(:);Y(:)]
uf=tabul(Z,"i")
u=uf(:,1)
f=uf(:,2)
q=size(uf,"r")
rr=1
for i=1:q
  j=find(Z==u(i))
  r(j)=mean(rr:rr+f(i)-1)
  rr=rr+f(i)
end
w2=sum(r(1:m))
if t=="<"
  pv=cdfwilcoxon2(w2,m,n)
elseif t==">"
  pv=1-cdfwilcoxon2(w2-1,m,n)
elseif t=="~"
  pv=cdfwilcoxon2(w2,m,n)
  pv=2*min(pv,1-pv)
else
  error("argument ''t'' must be ""<"", "">"" or ""~""")
end
endfunction