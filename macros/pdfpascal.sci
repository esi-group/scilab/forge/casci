//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfpascal(X,n,p)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if (p<=0)|(p>1)
  error("argument ''p'' must be in (0,1]")
end
Y=zeros(X)
if p==1
  Y(X==n)=1
else
  i=((X>=n)&(X==floor(X)))
  if or(i)
    Xi=X(i)
    Y(i)=exp(gammaln(Xi)+n*log(p)+(Xi-n)*log(1-p)-gammaln(n)-gammaln(Xi-n+1))
  end
end
endfunction