//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfkernel(X,Z,h,ker)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>4)
  error("incorrect number of arguments")
end
n=size(X,"*")
m=size(Z,"*")
if h<=0
  error("argument ''h'' must be  > 0")
end
if argin==3
  ker="epanechnikov"
end
Y=X
select ker
  case "uniform"
    c=1.7320508
    for j=1:n
      u=(X(j)-Z)/h
      v=zeros(u)
      i=find(abs(u)<c)
      v(i)=0.2886751
      Y(j)=sum(v)
    end
  case "triangular"
    c=2.4494897
    for j=1:n
      u=(X(j)-Z)/h
      v=zeros(u)
      i=find(abs(u)<c)
      v(i)=0.4082483*(1-abs(0.4082483*u(i)))
      Y(j)=sum(v)
    end
  case "epanechnikov"
    c=2.236068
    for j=1:n
      u=(X(j)-Z)/h
      v=zeros(u)
      i=find(abs(u)<c)
      v(i)=0.3354102*(1-0.2*u(i).^2)
      Y(j)=sum(v)
    end
  case "biweight"
    c=2.6457513
    for j=1:n
      u=(X(j)-Z)/h
      v=zeros(u)
      i=find(abs(u)<c)
      v(i)=0.3543417*(1-0.1428571*u(i).^2).^2
      Y(j)=sum(v)
    end
  case "triweight"
    c=3
    for j=1:n
      u=(X(j)-Z)/h
      v=zeros(u)
      i=find(abs(u)<c)
      v(i)=0.3645833*(1-0.1111111*u(i).^2).^3
      Y(j)=sum(v)
    end
  case "normal"
    for j=1:n
      u=(X(j)-Z)/h
      Y(j)=sum(pdfnormal(u))
    end
  else
    mprintf("argument ''ker'' must be ""uniform"", ""triangular"", ..
            ""epanechnikov""\n                       ""biweight"", ..
            ""triweight"" or ""normal""\n")
    error("")
end
Y=Y/(m*h)
endfunction