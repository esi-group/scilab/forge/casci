//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function V=varcovar(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
[n,p]=size(X)
Y=X-ones(n,1)*mean(X,"r")
V=(Y'*Y)/n
endfunction