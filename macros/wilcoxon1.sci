//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function y=cdfwilcoxon1(x,n)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
ma=n*(n+1)/2
m1=n*(n+1)/4
mu2=n*(n+1)*(2*n+1)/24
g2=-(12/5)*(3*n^2+3*n-1)/(n*(n+1)*(2*n+1))
b=sqrt(2*(g2-2-sqrt(4-7*g2))/g2)/2
d=sqrt(768*mu2*b^6/(48*b^4-24*b^2+17))
y=zeros(x)
i=find((x>=0)&(x<ma))
if i~=[]
  y(i)=cdfjohnson(floor(x(i))-m1+0.5,"B",0,b,-d/2,d)
end
i=find(x>=ma)
y(i)=1
endfunction
//-----------------------------------------------------------------------------
function pv=wilcoxon1(X,Y,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
X=X(:)
Y=Y(:)
n=length(X)
ny=length(Y)
if n~=ny
  error("arguments ''X'' and ''Y'' must have the same size")
end
if argin==2
  t="~"
end
Z=X-Y
k=find(Z>0)
Z=abs(Z)
uf=tabul(Z,"i")
u=uf(:,1)
f=uf(:,2)
m=size(uf,"r")
rr=1
for i=1:m
  j=find(clean(Z-u(i))==0)
  r(j)=mean(rr:rr+f(i)-1)
  rr=rr+f(i)
end
w1=sum(r(k))
if t=="<"
  pv=cdfwilcoxon1(w1,n)
elseif t==">"
  pv=1-cdfwilcoxon1(w1-1,n)
elseif t=="~"
  pv=cdfwilcoxon1(w1,n)
  pv=2*min(pv,1-pv)
else
  error("argument ''t'' must be ""<"", "">"" or ""~""")
end
endfunction