//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function f=fitgevo(par,X)
//-----------------------------------------------------------------------------
a=par(1)
b=par(2)
c=par(3)
Y=1+a*(X-c)/b
if (b<=0)|or(Y<=0)
  f=-%inf
else
  f=sum((-1-1/a)*log(Y)-Y.^(-1/a)-log(b))
end
endfunction
//-----------------------------------------------------------------------------
function [a,b,c]=fitgev(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
alpha=0.05
la=-log(alpha)
lm=-log(0.5)
lb=-log(1-alpha)
xa=quantile(X,alpha)
xm=median(X)
xb=quantile(X,1-alpha)
r=(xb-xm)/(xm-xa)
r0=(log(lb)-log(lm))/(log(lm)-log(la))
if r>r0
  a0=0
  a1=1
  while ((lb^(-a1)-lm^(-a1))/(lm^(-a1)-la^(-a1)))<r
    a0=a1
    a1=2*a1
  end
  while abs((a0-a1)/a1)>%eps
    a=(a0+a1)/2
    if ((lb^(-a)-lm^(-a))/(lm^(-a)-la^(-a)))<r
      a0=a
    else
      a1=a
    end
  end
else
  a0=0
  a1=-1
  while ((lb^(-a1)-lm^(-a1))/(lm^(-a1)-la^(-a1)))>r
    a0=a1
    a1=2*a1
  end
  while abs((a0-a1)/a1)>%eps
    a=(a0+a1)/2
    if ((lb^(-a)-lm^(-a))/(lm^(-a)-la^(-a)))>r
      a0=a
    else
      a1=a
    end
  end
end
b=a*(xb-xm)/(lb^(-a)-lm^(-a))
if a>0
  c=min(xa-b*(la^(-a)-1)/a,min(X)+b/a-0.1)
else
  c=max(xb-b*(lb^(-a)-1)/a,max(X)+b/a+0.1)
end
disp([a,b,c])
par=neldermead([a,b,c],fitgevo,list(X),tol=1e-8)
a=par(1)
b=par(2)
c=par(3)
endfunction