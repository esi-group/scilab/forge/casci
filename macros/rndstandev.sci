//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=rndstandev(sz,n,sigma)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
select size(sz,"*")
  case 1
    row=sz(1)
    col=1
  case 2
    row=sz(1)
    col=sz(2)
  else
    error("argument ''sz'' must contain a valid vector/matrix size")
end
if (row<=0)|(row~=floor(row))|(col<=0)|(col~=floor(col))
  error("argument ''sz'' must contain a valid vector/matrix size")
end
if (n<2)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 2")
end
if argin==2
  sigma=1
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
X=sigma*sqrt(grand(row,col,"gam",(n-1)/2,(n-1)/2))
endfunction