//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [mu,sd,sk,ku]=momdphase(Q,q)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
q=q(:)'
W=inv(eye(Q)-Q)
z=q*W
nu1=sum(z)
mu=nu1
if argout>=2
  WQ=W*Q
  z=z*WQ
  nu2=2*sum(z)
  sd=sqrt(nu2-nu1^2+nu1)
end
if argout>=3
  z=z*WQ
  nu3=6*sum(z)
  mu3=nu3+3*(1-nu1)*nu2+2*nu1^3-3*nu1^2+nu1
  sk=mu3/sd^3
end
if argout>=4
  z=z*WQ
  nu4=24*sum(z)
  mu4=nu4+6*nu3+nu1*(1-4*nu3)+(6*nu1^2-12*nu1+7)*nu2-3*nu1^4+6*nu1^3-4*nu1^2
  ku=mu4/sd^4-3
end
endfunction