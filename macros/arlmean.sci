//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function arl=arlmean(tau,n,K,side)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>4)
  error("incorrect number of arguments")
end
if or(tau<0)
  error("all elements of argument ''tau'' must be >= 0")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if K<0
  error("argument ''K'' must be >= 0")
end
if argin==3
  side="2sided"
end
if side=="1sided"
  pL=0
  pU=cdfnormal((tau-K)*sqrt(n))
elseif side=="2sided"
  pL=cdfnormal(-(K+tau)*sqrt(n))
  pU=cdfnormal((tau-K)*sqrt(n))
else
  error("argument ''side'' must be ""1sided"" or ""2sided""")
end
arl=(1)./(pU+pL)
endfunction