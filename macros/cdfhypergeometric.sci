//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfhypergeometric(X,n,p,N)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=4
  error("incorrect number of arguments")
end
if (N<=0)|(N~=floor(N))
  error("argument ''N'' must be an integer >= 1")
end
if (n<=0)|(n>N)|(n~=floor(n))
  error("argument ''n'' must be an integer in {1,..,N}")
end
if (p<0)|(p>1)
  error("argument ''p'' must be in [0,1]")
end
Np=N*p
if Np~=floor(Np)
  error("''N*p'' must be an integer >= 1")
end
mi=min(floor(min(X)),max(0,n-N*(1-p)))
ma=max(ceil(max(X)),min(n,N*p))
xx=mi:ma
yy=cumsum(pdfhypergeometric(xx,n,p,N))
for i=1:length(X)
  Y(i)=yy(min(find(X(i)<=xx)))
end
endfunction