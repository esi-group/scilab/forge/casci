//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=tstbinomial2(x,nx,y,ny,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<4)|(argin>5)
  error("incorrect number of arguments")
end
if (nx<1)|(nx~=floor(nx))
  error("argument ''nx'' must be an integer >= 1")
end
if (x<0)|(x~=floor(x))|(x>nx)
  error("argument ''x'' must be an integer in {0,...,nx}")
end
if (ny<1)|(ny~=floor(ny))
  error("argument ''ny'' must be an integer >= 1")
end
if (y<0)|(y~=floor(y))|(y>ny)
  error("argument ''y'' must be an integer in {0,...,ny}")
end
if argin==4
  t="~"
end
px=x/nx
py=y/ny
if px==py
  pv=1
else
  p=(nx*px+ny*py)/(nx+ny)
  if t=="<"
    pv=cdfnormal((px-py)/sqrt(p*(1-p)*(1/nx+1/ny)))
  elseif t==">"
    pv=cdfnormal((py-px)/sqrt(p*(1-p)*(1/nx+1/ny)))
  elseif t=="~"
    pv=cdfnormal((px-py)/sqrt(p*(1-p)*(1/nx+1/ny)))
    pv=2*min(pv,1-pv)
  else
    error("argument ''t'' must be ""<"", "">"" or ""~""")
  end
end
endfunction