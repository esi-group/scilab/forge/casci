//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function boxplot(varargin)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin==0
  error("incorrect number of arguments")
end
mi=+%inf
ma=-%inf
for i=1:argin
  xmin(i)=min(varargin(i))
  xmax(i)=max(varargin(i))
  mi=min(mi,xmin(i))
  ma=max(ma,xmax(i))
end
plot2d(0,0,0,rect=[0;mi-0.1*(ma-mi);argin+1;ma+0.1*(ma-mi)],..
             nax=[0,argin+2,0,10])
for i=1:argin
  x=varargin(i)
  x25=quantile(x,0.25)
  x50=quantile(x,0.5)
  x75=quantile(x,0.75)
  xrng=xmax(i)-xmin(i)
  IQR=x75-x25
  l1=x25-1.5*IQR
  l2=x25-3*IQR
  u1=x75+1.5*IQR
  u2=x75+3*IQR
  xlow=min(x(find((x<=x25)&(x>=l1))))
  xhigh=max(x(find((x>=x75)&(x<=u1))))
  y1=x(find(((x<=l1)&(x>=l2))|((x>=u1)&(x<=u2))))
  y2=x(find((x<l2)|(x>u2)))
  xrect(i-0.4,x75,0.8,x75-x25)
  xsegs([i-0.4,i,i-0.1,i,i-0.1;i+0.4,i,i+0.1,i,i+0.1],..
    [x50,x75,xhigh,x25,xlow;x50,xhigh,xhigh,xlow,xlow])
  if y1~=[]
    plot2d(i*ones(y1),y1,-5,nax=[0,argin+2,0,10])
  end
  if y2~=[]
    plot2d(i*ones(y2),y2,-4,nax=[0,argin+2,0,10])
  end
end
endfunction