//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function mulregdisp(res)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
alpha=0.05
mnp=res(1)
m=mnp(1)
n=mnp(2)
p=mnp(3)
a=res(2)
ye=res(3)
e=res(4)
SSR=res(5)
SSE=res(6)
R2=res(7)
R2pred=res(8)
MSR=res(9)
MSE=res(10)
R2a=res(11)
r=res(12)
D=res(13)
aL=res(14)
aU=res(15)
apv=res(16)
SSPE=res(17)
SSLOF=res(18)
MSPE=res(19)
MSLOF=res(20)
SLOF=res(21)
// COEFFICIENTS
mprintf("\nCOEFFICIENTS\n")
if n==p
  mprintf("     ------------\n")
  mprintf("        coeff    \n")
  mprintf("     ------------\n")
  mprintf("%3d  %12g\n",[(0:p-1)',a])
else
  mprintf("     ------------  --------------------------  -------\n")
  mprintf("        coeff        95\% confidence interval    p-val\n")
  mprintf("     ------------  --------------------------  -------\n")
  zer(1:p)=""
  zer(apv>=alpha)="= 0 ?"
  mprintf("%3d  %12g  %12g  %12g  %7.5f  %s\n",[(0:p-1)',a,aL,aU,apv],zer)
end
// RESIDUALS
mprintf("\nRESIDUALS\n")
if n==p
  mprintf("     ------------\n")
  mprintf("         res     \n")
  mprintf("     ------------\n")
  mprintf("%3d  %12g\n",[(1:n)',e])
else
  mprintf("     ------------  ------------  ------------\n")
  mprintf("         res         stud res        cook\n")
  mprintf("     ------------  ------------  ------------\n")
  out(1:n)=""
  out(2*cdfnormal(-abs(r))<alpha)=" OUTLIER ?"
  mprintf("%3d  %12g  %12g  %12g  %s\n",[(1:n)',e,r,D],out)
end
// SUM & MEAN OF SQUARES
mprintf("\nSUM & MEAN OF SQUARES\n")
mprintf("     ------------  ------------  ------------  -------  -------\n")
mprintf("          SSR           SSE          SST         R2      R2pred\n")
mprintf("     ------------  ------------  ------------  -------  -------\n")
mprintf("     %12g  %12g  %12g  %7.5f  %7.5f\n\n",[SSR,SSE,SSE+SSR,R2,R2pred])
if n>p
  mprintf("     ------------  ------------  ------------  -------\n")
  mprintf("          MSR           MSE        MSR/MSE       R2a\n")
  mprintf("     ------------  ------------  ------------  -------\n")
  mprintf("     %12g  %12g  %12g  %7.5f\n",[MSR,MSE,MSR/MSE,R2a])
end
// LACK OF FIT
if ((n>m)&(m>p))
  mprintf("\nLACK OF FIT\n")
  mprintf("     ------------  ------------\n")
  mprintf("         SSLOF         SSPE\n")
  mprintf("     ------------  ------------\n")
  mprintf("     %12g  %12g\n\n",[SSLOF,SSPE])
  mprintf("     ------------  ------------  ------------  -------\n")
  mprintf("         MSLOF         MSPE       MSLOF/MSPE     p-val\n")
  mprintf("     ------------  ------------  ------------  -------\n")
  if SLOF>=alpha
    mprintf("     %12g  %12g  %12g  %7.5f   FIT ACCEPTED !\n\n",..
            [MSLOF,MSPE,MSLOF/MSPE,SLOF])
  else
    mprintf("     %12g  %12ge %12g  %7.5f   FIT REJECTED !\n\n",..
            [MSLOF,MSPE,MSLOF/MSPE,SLOF])
  end
end
endfunction