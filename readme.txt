== CASCI Toolbox == 

This toolbox includes various functions for probability & statistics 
that are used by P. Castagliola's lab at Université de Nantes.

== Installation ==
Do this once inside casci folder, within scilab, to build the library:

  exec builder.sce

Every subsequent use of the library only requires

  exec loader.sce

to load the library whenever you need to use it.

== Documentation ==

See the file help/cascidoc.pdf for usage for each function independently.

== Author ==

Philippe CASTAGLIOLA
Professeur des Universités
Université de Nantes & IRCCyN UMR CNRS 6597
Institut Universitaire de Technologie de Nantes
Département Qualité Logistique Industrielle et Organisation
2 avenue du Professeur Jean Rouxel
BP 539 - 44475 Carquefou Cedex
Tel : (+33) 2 28 09 21 12
Fax : (+33) 2 28 09 21 01
EMail : philippe.castagliola@univ-nantes.fr
WWW : http://philippe.castagliola.free.fr/

== License ==

(c) 2012 Philippe Castagliola

This toolbox is released under the GPLv3.
Please contact the copyright holder for other licensing possibilities
