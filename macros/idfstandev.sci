//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=idfstandev(Y,n,sigma)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if or((Y<=0)|(Y>=1))
  error("all elements of argument ''Y'' must be in (0,1)")
end
if (n<2)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 2")
end
if argin==2
  sigma=1
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
X=sigma*sqrt(idfgamma(Y,(n-1)/2,2/(n-1)))
endfunction