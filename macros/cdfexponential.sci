//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfexponential(X,lam)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
if lam<=0
  error("argument ''lam'' must be > 0")
end
Y=zeros(X)
i=(X>=0)
if or(i)
  Y(i)=1-exp(-lam*X(i))
end
endfunction