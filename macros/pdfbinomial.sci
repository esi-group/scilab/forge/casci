//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfbinomial(X,n,p)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if (p<0)|(p>1)
  error("argument ''p'' must be in [0,1]")
end
Y=zeros(X)
if p==0
  i=(X==0)
  Y(i)=1
elseif p==1
  i=(X==n)
  Y(i)=1
else
  i=((X>=0)&(X<=n)&(X==floor(X)))
  if or(i)
    Xi=X(i)
    Y(i)=exp(gammaln(n+1)+Xi*log(p)+(n-Xi)*log(1-p)-..
             gammaln(Xi+1)-gammaln(n-Xi+1))
  end
end
endfunction