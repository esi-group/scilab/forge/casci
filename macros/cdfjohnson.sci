//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfjohnson(X,s,a,b,c,d)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=6
  error("incorrect number of arguments")
end
if b<=0   
  error("argument ''b'' must be > 0")
end
if d<=0
  error("argument ''d'' must be > 0")
end
if s=="U"
  Y=cdfnormal(a+b*asinh((X-c)/d))
elseif s=="B"
  Y=zeros(X)
  i=((c<X)&(X<(c+d)))
  if or(i)
    Xi=X(i)
    Y(i)=cdfnormal(a+b*log((Xi-c)./(c+d-Xi)))
  end
  Y((c+d)<=X)=1
else
  error("argument ''s'' must be ""B"" or ""U""")
end
endfunction