//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=idfweibull(Y,a,b,c)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>4)
  error("incorrect number of arguments")
end
if or((Y<=0)|(Y>=1))
  error("all elements of argument ''Y'' must be in (0,1)")
end
if a<=0
  error("argument ''a'' must be > 0")
end
if ~exists("b","local")
  b=1
end
if ~exists("c","local")
  c=0
end
if b<=0
  error("argument ''b'' must be > 0")
end
X=c+b*(-log(1-Y)).^(1/a)
endfunction