//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=rndjohnson(sz,s,a,b,c,d)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=6
  error("incorrect number of arguments")
end
select size(sz,"*")
  case 1
    row=sz(1)
    col=1
  case 2
    row=sz(1)
    col=sz(2)
  else
    error("argument ''sz'' must contain a valid vector/matrix size")
end
if (row<=0)|(row~=floor(row))|(col<=0)|(col~=floor(col))
  error("argument ''sz'' must contain a valid vector/matrix size")
end
if b<=0   
  error("argument ''b'' must be > 0")
end
if d<=0
  error("argument ''d'' must be > 0")
end
if s=="U"
  X=c+d*sinh((grand(row,col,"nor",0,1)-a)/b)
elseif s=="B"
  X=c+d./(exp((a-grand(row,col,"nor",0,1))/b)+1)
else
  error("argument ''s'' must be ""B"" or ""U""")
end
endfunction