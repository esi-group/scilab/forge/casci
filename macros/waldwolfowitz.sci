//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=waldwolfowitz(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
if or((X~=0)&(X~=1))
  error("argument ''X'' must matrix of {0,1}")
end
n0=size(find(X==0),"*")
n1=size(find(X==1),"*")
n=n0+n1
j=2
run=0
for i=1:n
  if X(i)~=j
    run=run+1
    j=X(i)
  end
end
mu=1+2*n0*n1/n
sigma=sqrt((mu-1)*(mu-2)/(n-1))
pv=1-cdfnormal(abs((run-mu)/sigma))
endfunction