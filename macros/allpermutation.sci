//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//----------------------------------------------------------------------------
function P=allpermutation(X)
//----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
if X==[]
  P=[]
else
  X=X(:)'
  P=allpermutationr(X)
end
endfunction
//----------------------------------------------------------------------------
function P=allpermutationr(X)
//----------------------------------------------------------------------------
n=length(X)
if n==1
  P=X
else
  P=[]
  for i=1:n
    P1=allpermutationr(X([1:i-1,i+1:n]))
    P=[P;X(i)*ones(size(P1,"r"),1),P1]
  end
end
endfunction