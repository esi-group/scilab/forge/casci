//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=tstnormals2(X,Y,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if argin==2
  t="~"
end
nx=size(X,"*")
ny=size(Y,"*")
sx=standev(X)
sy=standev(Y)
if t=="<"
  pv=cdffisher((sx/sy)^2,nx-1,ny-1)
elseif t==">"
  pv=1-cdffisher((sx/sy)^2,nx-1,ny-1)
elseif t=="~"
  pv=cdffisher((sx/sy)^2,nx-1,ny-1)
  pv=2*min(pv,1-pv)
else
  error("argument ''t'' must be ""<"", "">"" or ""~""")
end
endfunction