//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=bartlett(varargin)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin<2
  error("incorrect number of arguments")
end
k=argin
for i=1:k
  X=varargin(i)
  n(i)=size(X,"*")
  s2(i)=sum((X-mean(X)).^2)/(n(i)-1)
end
N=sum(n)
s2p=sum((n-1).*s2)/(N-k)
t=((N-k)*log(s2p)-sum((n-1).*log(s2)))/..
  (1+(sum((1)./(n-1))-1/(N-k))/(3*(k-1)))
pv=1-cdfchi2(t,k-1)
endfunction