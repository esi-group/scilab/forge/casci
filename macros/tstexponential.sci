//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=tstexponential(X,lam0,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if lam0<=0
  error("argument ''lam0'' must be > 0")
end
if argin==2
  t="~"
end
n=size(X,"*")
if t=="<"
  pv=1-cdfchi2(2*lam0*sum(X),2*n)
elseif t==">"
  pv=cdfchi2(2*lam0*sum(X),2*n)
elseif t=="~"
  pv=cdfchi2(2*lam0*sum(X),2*n)
  pv=2*min(pv,1-pv)
else
  error("argument ''t'' must be ""<"", "">"" or ""~""")
end
endfunction