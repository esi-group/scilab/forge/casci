//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function f=fitweibullo(par,X)
//-----------------------------------------------------------------------------
a=par(1)
b=par(2)
c=par(3)
Y=(X-c)/b
if (a<=0)|(b<=0)|or(Y<=0)
  f=-%inf
else
  f=sum(log(a/b)+(a-1)*log(Y)-Y.^a)
end
endfunction
//-----------------------------------------------------------------------------
function [a,b,c]=fitweibull(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
alpha=0.05
la=-log(alpha)
lm=-log(0.5)
lb=-log(1-alpha)
xa=quantile(X,alpha)
xm=median(X)
xb=quantile(X,1-alpha)
r=(xb-xm)/(xm-xa)
a0=0
a1=1
while ((la^(1/a1)-lm^(1/a1))/(lm^(1/a1)-lb^(1/a1)))>r
  a0=a1
  a1=2*a1
end
while abs(a0-a1)/a1>%eps
  a=(a0+a1)/2
  if ((la^(1/a)-lm^(1/a))/(lm^(1/a)-lb^(1/a)))>r
    a0=a
  else
    a1=a
  end
end
b=(xb-xm)/(la^(1/a)-lm^(1/a))
c=min(xa-b*lb^(1/a),min(X)-0.1)
par=neldermead([a,b,c],fitweibullo,list(X),tol=1e-8)
a=par(1)
b=par(2)
c=par(3)
endfunction