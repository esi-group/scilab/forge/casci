//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfstudent(X,n,nc)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if argin==2
  nc=0
end
if nc==0
  Y=cdft("PQ",X,n*ones(X))
else
  Y=cdftnc("PQ",X,n*ones(X),nc*ones(X))
end
endfunction