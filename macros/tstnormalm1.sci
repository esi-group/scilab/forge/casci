//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=tstnormalm1(X,mu0,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if argin==2
  t="~"
end
n=size(X,"*")
mu=mean(X)
sd=standev(X)
if t=="<"
  pv=cdfstudent(sqrt(n)*(mu-mu0)/sd,n-1)
elseif t==">"
  pv=1-cdfstudent(sqrt(n)*(mu-mu0)/sd,n-1)
elseif t=="~"
  pv=cdfstudent(sqrt(n)*(mu-mu0)/sd,n-1)
  pv=2*min(pv,1-pv)
else
  error("argument ''t'' must be ""<"", "">"" or ""~""")
end
endfunction