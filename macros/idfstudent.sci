//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=idfstudent(Y,n,nc)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if or((Y<=0)|(Y>=1))
  error("all elements of argument ''Y'' must be in (0,1)")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if argin==2
  nc=0
end
if nc==0
  X=cdft("T",n*ones(Y),Y,1-Y)
else
  X=cdftnc("T",n*ones(Y),nc*ones(Y),Y,1-Y)
end
endfunction