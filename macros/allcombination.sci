//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//----------------------------------------------------------------------------
function C=allcombination(p,X)
//----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
if X==[]
  C=[]
else
  X=X(:)'
  n=length(X)
  if (p<=0)|(p~=floor(p))|(p>n)
    error("argument ''p'' must be an integer in {1,2,...,''n''}")
  end
  C=allcombinationr(p,X)
end
endfunction
//----------------------------------------------------------------------------
function C=allcombinationr(p,X)
//----------------------------------------------------------------------------
if p==1
  C=X'
else
  n=length(X)
  C=[]
  for i=1:(n-p+1)
    C1=allcombinationr(p-1,X([i+1:n]))
    C=[C;X(i)*ones(size(C1,"r"),1),C1]
  end
end
endfunction