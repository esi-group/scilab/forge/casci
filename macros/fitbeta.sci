//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function f=fitbetab(par,sk,ku)
//-----------------------------------------------------------------------------
a=par(1)
b=par(2)
if (a<=0)|(b<=0)
  f=[%inf,%inf]
else
  f(1)=sk-2*(b-a)*sqrt((a+b+1)/(a*b))/(a+b+2)
  f(2)=ku-6*((a+b)^2*(a+b+1)-a*b*(5*(a+b)+6))/(a*b*(a+b+2)*(a+b+3))
end
endfunction
//-----------------------------------------------------------------------------
function f=fitbetao(par,X)
//-----------------------------------------------------------------------------
a=par(1)
b=par(2)
c=par(3)
d=par(4)
Y=X-c
Z=c+d-X
if (a<=0)|(b<=0)|(d<=0)|or(Y<=0)|or(Z<=0)
  f=-%inf
else
  f=sum(gammaln(a+b)+(a-1)*log(Y)+(b-1)*log(Z)-..
               (a+b-1)*log(d)-gammaln(a)-gammaln(b))
end
endfunction
//-----------------------------------------------------------------------------
function [a,b,c,d]=fitbeta(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
sk=skewness(X)
ku=kurtosis(X)
par=simplexolve([1,1],fitbetab,list(sk,ku),tol=1e-8)
a=par(1)
b=par(2)
c=min(X)-0.1
d=rnge(X)+0.2
par=neldermead([a,b,c,d],fitbetao,list(X),tol=1e-8)
a=par(1)
b=par(2)
c=par(3)
d=par(4)
endfunction