//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [Cpk,inter]=cpk(X,L,U,side,level)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>5)
  error("incorrect number of arguments")
end
if L>=U
  error("argument ''L'' must be < argument ''U''")
end
mu=mean(X)
Sp=standev(X)
Cpk=min(U-mu,mu-L)/(3*Sp)
if argout==2
  if ~exists("side","local")
    side="both"
  end
  if ~exists("level","local")
    level=0.95
  end
  if (level<=0.5)|(level>=1)
    error("argument ''level'' must be in (0.5,1)")
  end
  alpha=1-level
  n=size(X,"*")
  select side
    case "lower"
      inter=Cpk-idfnormal(1-alpha)*sqrt(((n-1)/(9*n*(n-3)))+..
                                         (Cpk^2/(2*(n-3)))*(1+6/(n-1)))
    case "upper"
      inter=Cpk+idfnormal(1-alpha)*sqrt(((n-1)/(9*n*(n-3)))+..
                                         (Cpk^2/(2*(n-3)))*(1+6/(n-1)))
    case "both"
      inter(1)=Cpk-idfnormal(1-alpha/2)*sqrt(((n-1)/(9*n*(n-3)))+..
                                         (Cpk^2/(2*(n-3)))*(1+6/(n-1)))
      inter(2)=Cpk+idfnormal(1-alpha/2)*sqrt(((n-1)/(9*n*(n-3)))+..
                                         (Cpk^2/(2*(n-3)))*(1+6/(n-1)))
    else
      error("argument ''side'' must be ''lower'', ''upper'' or ''both''")
  end
end
endfunction