//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfcpmk(X,mut,sit,n)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=4
  error("incorrect number of arguments")
end
if sit<=0
  error("argument ''sit'' must be > 0")
end
if (n<=1)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 2")
end
Y=pdfcpuv(X,1,1,mut,sit,n)
endfunction