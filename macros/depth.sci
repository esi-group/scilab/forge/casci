//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [d,id]=depth(x,dep,y)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
[nx,px]=size(x)
onx=ones(nx,1)
if argin==2
  select dep
    case "halfspace"
      opx=ones(px,1)
      c=allcombination(px,1:nx)
      m=size(c,"r")
      d=%inf*onx
      for i=1:m
        xi=x(c(i,:),:)
        a=[1;xi\(-opx)]
        u=[onx,x]*a
        u(c(i,:))=0
        jn=find(u<0)
        if jn~=[]
          sjn=length(jn)
          d(jn)=min(d(jn),sjn)
        end
        jp=find(u>0)
        if jp~=[]
          sjp=length(jp)
          d(jp)=min(d(jp),sjp)
        end
      end
    case "majority"
      opx=ones(px,1)
      c=allcombination(px,1:nx)
      m=size(c,"r")
      d=zeros(nx,1)
      for i=1:m
        xi=x(c(i,:),:)
        a=[1;xi\(-opx)]
        u=[onx,x]*a
        u(c(i,:))=0
        jn=find(u<0)
        sjn=length(jn)
        jp=find(u>0)
        sjp=length(jp)
        if sjn>sjp
          d(jn)=d(jn)+1
        elseif sjn<sjp
          d(jp)=d(jp)+1
        end
      end
    case "simplicial"
      opx=ones(px+1,1)
      c=allcombination(px+1,1:nx)
      m=size(c,"r")
      d=zeros(nx,1)
      for i=1:m
        xi=x(c(i,:),:)
        z=[x,onx]/[xi,opx]
        u=and(z<1,"c")
        u(c(i,:))=%f
        j=find(u)
        d(j)=d(j)+1
      end
    else
      error("argument ''dep'' must be ""halfspace"", ""majority"" or ""simplicial""")
  end
else
  [ny,py]=size(y)
  if px~=py
    error("arguments ''x'' and ''y'' have incompatible sizes")
  end
  ony=ones(ny,1)
  select dep
    case "halfspace"
      opx=ones(px,1)
      c=allcombination(px,1:nx)
      m=size(c,"r")
      d=%inf*ony
      for i=1:m
        xi=x(c(i,:),:)
        a=[1;xi\(-opx)]
        ux=[onx,x]*a
        ux(c(i,:))=0
        uy=[ony,y]*a
        jyn=find(uy<0)
        if jyn~=[]
          jxn=find(ux<0)
          sxjn=length(jxn)
          d(jyn)=min(d(jyn),sxjn)
        end
        jyp=find(uy>0)
        if jyp~=[]
          jxp=find(ux>0)
          sxjp=length(jxp)
          d(jyp)=min(d(jyp),sxjp)
        end
      end
    case "majority"
      opx=ones(px,1)
      c=allcombination(px,1:nx)
      m=size(c,"r")
      d=zeros(ny,1)
      for i=1:m
        xi=x(c(i,:),:)
        a=[1;xi\(-opx)]
        ux=[onx,x]*a
        ux(c(i,:))=0
        uy=[ony,y]*a
        jxn=find(ux<0)
        sjxn=length(jxn)
        jxp=find(ux>0)
        sjxp=length(jxp)
        if sjxn>sjxp
          jyn=find(uy<0)
          if jyn~=[]
            d(jyn)=d(jyn)+1
          end
        elseif sjxn<sjxp
          jyp=find(uy>0)
          if jyp~=[]
            d(jyp)=d(jyp)+1
          end
        end
      end
    case "simplicial"
      opx=ones(px+1,1)
      c=allcombination(px+1,1:nx)
      m=size(c,"r")
      d=zeros(ny,1)
      for i=1:m
        xi=x(c(i,:),:)
        z=[y,ony]/[xi,opx]
        u=and(z<1,"c")
        u(c(i,:))=%f
        j=find(u)
        d(j)=d(j)+1
      end
    else
      error("argument ''dep'' must be ""halfspace"", ""majority"" or ""simplicial""")
  end
end
if argout==2
  [sd,id]=sort(d)
end
endfunction