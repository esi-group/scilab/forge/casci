//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfpascal(X,n,p)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if (p<=0)|(p>1)
  error("argument ''p'' must be in (0,1]")
end
Y=zeros(X)
i=(X>=n)
if or(i)
  Xi=floor(X(i))
  Y(i)=cdfnbn("PQ",Xi-n,n*ones(Xi),p*ones(Xi),(1-p)*ones(Xi))
end
endfunction