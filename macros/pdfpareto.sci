//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfpareto(X,a,b,c)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>4)
  error("incorrect number of arguments")
end
if ~exists("b","local")
  b=1
end
if ~exists("c","local")
  c=0
end
if b<=0
  error("argument ''b'' must be > 0")
end
Y=zeros(X)
if a==0
  i=(X>=c)
  if or(i)
    Y(i)=exp(-(X(i)-c)/b)/b
  end
else
  if a>0
    i=(X>=c)
  else
    i=((c<=X)&(X<c-b/a))
  end
  if or(i)
    Y(i)=((1+a*(X(i)-c)/b).^(-1/a-1))/b
  end  
end
endfunction