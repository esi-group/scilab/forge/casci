//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Z=centralcomposite(k,delta,n0)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>3)
  error("incorrect number of arguments")
end
select k
  case 2
    Y1=factorial2(2)
  case 3
    Y1=factorial2(3)
  case 4
    Y1=factorial2(4)
  case 5
    Y1=factorial2(5,"+ABCD")
  case 6
    Y1=factorial2(6,"+ABCDE")
  case 7
    Y1=factorial2(7,"+ABCDEF")
  case 8
    Y1=factorial2(8,["+ABCD","+ABEF"])
  case 9
    Y1=factorial2(9,["+ACDFG","+BCEFG"])
  case 10
    Y1=factorial2(10,["+ABCG","+ACDE","+ACDF"])
  case 11
    Y1=factorial2(11,["+ABCG","+BCDE","+ACDF","+ABCDEFG"])
  else
    error("argument ''k'' must be an integer in 2,...,11")
end
if ~exists("delta","local")
  delta=(size(Y1,"r"))^(0.25)
end
if delta<0
  error("argument ''delta'' must be > 0")
end
for i=1:k
  Y2(2*i-1,i)=-delta
  Y2(2*i,i)=delta
end
if ~exists("n0","local")
  n0=1
end
if (n0<0)|(n0~=floor(n0))
  error("argument ''n0'' must be an integer >= 0")
end
Z=[Y1;Y2;zeros(n0,k)]
endfunction