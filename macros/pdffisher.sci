//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdffisher(X,m,n,nc)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>4)
  error("incorrect number of arguments")
end
if (m<=0)|(m~=floor(m))
  error("argument ''m'' must be an integer >= 1")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if argin==3
  nc=0
end
if nc<0
  error("argument ''nc'' must be >= 0")
end
Y=zeros(X)
i=(X>0)
if or(i)
  n2=n/2
  m2=m/2
  Xi=X(i)
  if nc==0
    Y(i)=exp(gammaln(m2+n2)-gammaln(m2)-gammaln(n2)+m2*log(m)+n2*log(n)+(m2-1)*log(Xi)-(m2+n2)*log(m*Xi+n))
  else
    lm=log(m)
    nc2=nc/2
    a=n2*log(n)-nc2-gammaln(n2)
    lnc2=log(nc2)
    lXi=log(Xi)
    lnmXi=log(n+m*Xi)
    k=0
    while %t
      m2k=m2+k
      n2m2k=n2+m2k
      Zi=exp(a+k*lnc2+m2k*lm+(m2k-1)*lXi-n2m2k*lnmXi+gammaln(n2m2k)-gammaln(m2k)-gammaln(k+1))
      if max(Zi)<%eps
        break
      end
      Y(i)=Y(i)+Zi
      k=k+1
    end
  end
end
endfunction