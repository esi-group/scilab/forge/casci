//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfchi2(X,n,nc)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if argin==2
  nc=0
end
if nc<0
  error("argument ''nc'' must be >= 0")
end
Y=zeros(X)
i=(X>0)
if or(i)
  Xi=X(i)
  if nc==0
    Y(i)=cdfchi("PQ",Xi,n*ones(Xi))
  else
    Y(i)=cdfchn("PQ",Xi,n*ones(Xi),nc*ones(Xi))
  end
end
endfunction