//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=tstnormals1(X,sig0,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if sig0<=0
  error("argument ''sig0'' must be > 0")
end
if argin==2
  t="~"
end
n=size(X,"*")
sig=standev(X)
if t=="<"
  pv=cdfchi2((n-1)*(sig/sig0)^2,n-1)
elseif t==">"
  pv=1-cdfchi2((n-1)*(sig/sig0)^2,n-1)
elseif t=="~"
  pv=cdfchi2((n-1)*(sig/sig0)^2,n-1)
  pv=2*min(pv,1-pv)
else
  error("argument ''t'' must be ""<"", "">"" or ""~""")
end
endfunction