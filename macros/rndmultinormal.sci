//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=rndmultinormal(n,mu,sigma)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
[rm,cm]=size(mu)
if rm~=1
  error("argument ''mu'' must be a row vector")
end
if argin==2
  sigma=eye(cm,cm)
else
  [rs,cs]=size(sigma)
  if rs==1
    if cm~=cs
      error("argument ''mu'' and argument ''sigma'' have incompatible sizes")
    else
      sigma=diag(sigma)
    end
  else
    if rs~=cs
      error("argument ''sigma'' must be a square positive definite matrix")
    end
    if cm~=cs
      error("argument ''mu'' and argument ''sigma'' have incompatible sizes")
    end
  end
end
X=grand(n,"mn",mu',sigma)' 
endfunction