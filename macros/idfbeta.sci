//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=idfbeta(Y,a,b,c,d)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>5)
  error("incorrect number of arguments")
end
if or((Y<=0)|(Y>=1))
  error("all elements of argument ''Y'' must be in (0,1)")
end
if a<=0
  error("argument ''a'' must be > 0")
end
if b<=0
  error("argument ''b'' must be > 0")
end
if ~exists("c","local")
  c=0
end
if ~exists("d","local")
  d=1
end
if d<=0
  error("argument ''d'' must be > 0")
end
X=c+d*cdfbet("XY",a*ones(Y),b*ones(Y),Y,1-Y)
endfunction