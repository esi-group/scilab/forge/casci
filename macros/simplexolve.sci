//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function xsol=simplexolve(x0,fun,extra,tol)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>4)
  error("incorrect number of arguments")
end
[j,p]=size(x0)
if j~=1
  error("argument ''x0'' must be a row vector")
end
if ~exists("extra","local")
  extra=list()
end
if ~exists("tol","local")
  tol=1e-12
end
if typeof(fun)~="function"
  error("argument ''fun'' must be a function")
end
if typeof(extra)~="list"
  error("argument ''extra'' must be a list")
end
if tol<=0
  error("argument ''tol'' must be > 0")
end
r=1
while %t
  X=simplex(x0,r)
  for i=1:p+1
    f(i)=norm(fun(X(i,:),extra(:)))
  end
  if and(isinf(f))
    r=r/2
  else
    break
  end
end
onesp1=ones(p+1,1)
while %t
  [f,j]=sort(f)
  X=X(j,:)
  fsol=f(p+1)
  xsol=X(p+1,:)
  fw=f(1)
  xw=X(1,:)
  xm=mean(X(2:p+1,:),"r")
  if sqrt(sum((xw-xm).^2))<=tol
    break
  end
  d=xm-xw
  xr=xw+2*d
  fr=norm(fun(xr,extra(:)))
  if fr<fsol
    xe=xr+d
    fe=norm(fun(xe,extra(:)))
    if fe<fr
      f(1)=fe
      X(1,:)=xe
    else
      f(1)=fr
      X(1,:)=xr
    end
  else
    xc=xw+0.5*d
    fc=norm(fun(xc,extra(:)))
    if fc<fsol
      f(1)=fc
      X(1,:)=xc
    else
      X=0.5*(X+onesp1*xsol)
      for i=1:p
        f(i)=norm(fun(X(i,:),extra(:)))
      end
    end
  end
end
endfunction