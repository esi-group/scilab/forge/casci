//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfrnge(X,n,sigma)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if (n<2)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 2")
end
if argin==2
  sigma=1
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
X=X/sigma
g=[0.4947004675, 0.4722875115, 0.4328156012, 0.3777022042, ..
   0.3089381222, 0.2290083888, 0.1408017754, 0.04750625492]
h=[0.01357622971, 0.03112676197, 0.04757925584, 0.06231448563, ..
   0.07479799441, 0.08457825969, 0.09130170752, 0.09472530523]
Y=zeros(X)
i=(X>0)
if or(i)
  Xi=X(i)
  Xl=0.5*Xi
  a=0.5*(8+Xl)
  b=8-Xl
  YY=0
  for k=1:8
    c=b*g(k)
    XX=a+c
    X1=0.3989422804*exp(-0.5*(XX.*XX)).*(cdfnormal(XX)-cdfnormal(XX-Xi)).^(n-1)
    XX=a-c
    X2=0.3989422804*exp(-0.5*(XX.*XX)).*(cdfnormal(XX)-cdfnormal(XX-Xi)).^(n-1)
    YY=YY+h(k)*(X1+X2)
  end
  Y(i)=(2*(cdfnormal(Xl)-0.5)).^n+2*n*(b.*YY)
end
endfunction