//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function r=boxcoxlinearo(lam,x,y,yybar)
//-----------------------------------------------------------------------------
if lam==0
  x=log(x)
else
  x=(x.^lam-1)/lam
end
xxbar=x-mean(x)
r=sum(xxbar.*yybar)/sqrt(sum(xxbar.^2))
endfunction
//-----------------------------------------------------------------------------
function [lam,rmax]=boxcoxlinear(x,y)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
x=x(:)
y=y(:)
if length(x)~=length(y)
  error("arguments ''x'' and ''y'' have incompatible sizes")
end
if or(x<=0)
  error("entries of argument ''x'' must be > 0")
end
yybar=y-mean(y)
[lam,r]=neldermead(0,boxcoxlinearo,list(x,y,yybar),tol=1e-6)
rmax=r/sqrt(sum(yybar.^2))
endfunction