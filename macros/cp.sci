//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [Cp,inter]=cp(X,L,U,side,level)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>5)
  error("incorrect number of arguments")
end
if L>=U
  error("argument ''L'' must be < argument ''U''")
end
Sp=standev(X)
Cp=(U-L)/(6*Sp)
if argout==2
  if ~exists("side","local")
    side="both"
  end
  if ~exists("level","local")
    level=0.95
  end
  if (level<=0.5)|(level>=1)
    error("argument ''level'' must be in (0.5,1)")
  end
  alpha=1-level
  n=size(X,"*")
  select side
    case "lower"
      inter=Cp*sqrt(idfchi2(alpha,n-1)/(n-1))
    case "upper"
      inter=Cp*sqrt(idfchi2(1-alpha,n-1)/(n-1))
    case "both"
      inter(1)=Cp*sqrt(idfchi2(alpha/2,n-1)/(n-1))
      inter(2)=Cp*sqrt(idfchi2(1-alpha/2,n-1)/(n-1))
    else
      error("argument ''side'' must be ''lower'', ''upper'' or ''both''")
  end
end
endfunction