//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function y=pdfmultinormal(X,mu,sigma)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
[n,p]=size(X)
[rm,cm]=size(mu)
if rm~=1
  error("argument ''mu'' must be a row vector")
end
if p~=cm
  error("argument ''X'' and argument ''mu'' have incompatible sizes")
end
if argin==2
  sigma=eye(p,p)
  isigma=sigma
  detsigma=1
else
  [rs,cs]=size(sigma)
  if rs==1
    if p~=cs
      error("argument ''X'' and argument ''sigma'' have incompatible sizes")
    else
      isigma=diag((1)./sigma)
      detsigma=prod(sigma)
      sigma=diag(sigma)
    end
  else
    if rs~=cs
      error("argument ''sigma'' must be a square positive definite matrix")
    end
    if p~=cs
      error("argument ''X'' and argument ''sigma'' have incompatible sizes")
    end
    isigma=inv(sigma) 
    detsigma=det(sigma)
  end
end
for i=1:n
  z=X(i,:)-mu
  y(i)=exp(-z*isigma*z'/2)/sqrt(2*%pi*detsigma)
end
endfunction