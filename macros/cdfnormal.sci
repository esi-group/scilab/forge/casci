//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfnormal(X,mu,sigma)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>3)
  error("incorrect number of arguments")
end
if ~exists("mu","local")
  mu=0
end
if ~exists("sigma","local")
  sigma=1
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
Y=cdfnor("PQ",X,mu*ones(X),sigma*ones(X))
endfunction