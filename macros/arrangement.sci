//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function A=arrangement(N,P)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
if or(size(N)~=size(P))
  error("arguments ''N'' and ''P'' have incompatible sizes")
end
if or((N<=0)|(N~=floor(N)))
  error("argument ''N'' must be a matrix of integers >= 1")
end
if or((P<0)|(P~=floor(P)))
  error("argument ''P'' must be a matrix of integers >= 0")
end
if or(P>N)
  error("entries of argument ''N'' must be >= argument ''P''")
end
A=exp(gammaln(N+1)-gammaln(N-P+1))
endfunction