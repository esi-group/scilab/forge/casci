//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function ku=kurtosis(X,cr)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if argin==1
  ku=mean(((X-mean(X))/standev(X)).^4)-3
elseif cr=="c"
  co=size(X,"c")
  ku=mean(((X-(mean(X,"c")*ones(1,co)))./(standev(X,"c")*ones(1,co))).^4,"c")-3
elseif cr=="r"
  ro=size(X,"r")
  ku=mean(((X-(ones(ro,1)*mean(X,"r")))./(ones(ro,1)*standev(X,"r"))).^4,"r")-3
else
  error("argument ''cr'' must be ""c"" or ""r""")
end
endfunction