//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Cp=mcpshahriari(X,L,U)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
[n,p]=size(X)
L=L(:)
pL=size(L,"r")
if p~=pL
  error("argument ''X'' and argument ''L'' have incompatible sizes")
end
U=U(:)
pU=size(U,"r")
if p~=pU
  error("argument ''X'' and argument ''U'' have incompatible sizes")
end
mu=mean(X,"r")
iS=inv(varcovar(X))
dis=det(iS)
idc=idfchi2(0.9973,p)
for i=1:p
  t=[1:i-1,i+1:p]
  w=sqrt(idc*det(iS(t,t))/dis)
  LP(i)=mu(i)-w
  UP(i)=mu(i)+w
end
Cp=(prod(U-L)/prod(UP-LP))^(1/p)
endfunction