//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function rg=rnge(X,cr)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if argin==1
  rg=max(X)-min(X)
elseif (cr=="c")|(cr=="r")
  rg=max(X,cr)-min(X,cr)
else
  error("argument ''cr'' must be ""c"" or ""r""")
end
endfunction