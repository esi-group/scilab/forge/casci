//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [pvsk,pvku,pvsku]=tstsku(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
n=size(X,"*")
dsk=skewness(X)/sqrt(6*n*(n-1)/((n-2)*(n+1)*(n+3)))
dku=kurtosis(X)/sqrt(24*n*(n-1)^2/((n-3)*(n-2)*(n+3)*(n+5)))
pvsk=2*(1-cdfnormal(abs(dsk)))
pvku=2*(1-cdfnormal(abs(dku)))
pvsku=1-cdfchi2(dsk^2+dku^2,2)
endfunction