//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [xopt,fopt]=torczon(x0,fun,extra,tol,opt)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>5)
  error("incorrect number of arguments")
end
[j,p]=size(x0)
if j~=1
  error("argument ''x0'' must be a row vector")
end
if ~exists("extra","local")
  extra=list()
end
if ~exists("tol","local")
  tol=1e-12
end
if ~exists("opt","local")
  opt="max"
end
if typeof(fun)~="function"
  error("argument ''fun'' must be a function")
end
if typeof(extra)~="list"
  error("argument ''extra'' must be a list")
end
if tol<=0
  error("argument ''tol'' must be > 0")
end
onesp1=ones(p+1,1)
r=1
while %t
  X=simplex(x0,r)
  for i=1:p+1
    f(i)=fun(X(i,:),extra(:))
  end
  if and(isinf(f))
    r=r/2
  else
    break
  end
end
if opt=="max"
  [fmax,kmax]=max(f)
  while r>tol
    Xr=2*onesp1*X(kmax,:)-X
    fr(kmax)=f(kmax)
    kk=[1:kmax-1,kmax+1:p+1]
    for i=kk
      fr(i)=fun(Xr(i,:),extra(:))
    end
    [frmax,krmax]=max(fr)
    if frmax>fmax
      Xe=2*Xr-onesp1*X(kmax,:)
      fe(kmax)=f(kmax)
      for i=kk
        fe(i)=fun(Xe(i,:),extra(:))
      end
      [femax,kemax]=max(fe)
      if femax>frmax
        X=Xe
        fmax=femax
        kmax=kemax
       r=r*2
      else
        X=Xr
        fmax=frmax
        kmax=krmax
      end
    else
      X=(X+onesp1*X(kmax,:))/2
      for i=kk
        f(i)=fun(X(i,:),extra(:))
      end
      [fmax,kmax]=max(f)
      r=r/2
    end
  end
  xopt=X(kmax,:)
  fopt=fmax
elseif opt=="min"
  [fmin,kmin]=min(f)
  while r>tol
    Xr=2*onesp1*X(kmin,:)-X
    fr(kmin)=f(kmin)
    kk=[1:kmin-1,kmin+1:p+1]
    for i=kk
      fr(i)=fun(Xr(i,:),extra(:))
    end
    [frmin,krmin]=min(fr)
    if frmin<fmin
      Xe=2*Xr-onesp1*X(kmin,:)
      fe(kmin)=f(kmin)
      for i=kk
        fe(i)=fun(Xe(i,:),extra(:))
      end
      [femin,kemin]=min(fe)
      if femin<frmin
        X=Xe
        fmin=femin
        kmin=kemin
        r=r*2
      else
        X=Xr
        fmin=frmin
        kmin=krmin
      end
    else
      X=(X+onesp1*X(kmin,:))/2
      for i=kk
        f(i)=fun(X(i,:),extra(:))
      end
      [fmin,kmin]=min(f)
      r=r/2
    end
  end
  xopt=X(kmin,:)
  fopt=fmin
else
  error("argument ''opt'' must be ""min"" or ""max""")
end
endfunction