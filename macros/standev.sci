//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function sd=standev(X,cr)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if argin==1
  n=size(X,"*")
  if n<2
    error("argument ''X'' must contain at least 2 entries")
  end
  sd=sqrt(sum((X-mean(X)).^2)/(n-1))
elseif cr=="c"
  n=size(X,"c")
  if n<2
    error("argument ''X'' must contain at least 2 entries per row")
  end
  sd=sqrt(sum((X-mean(X,"c")*ones(1,n)).^2,"c")/(n-1))
elseif cr=="r"
  n=size(X,"r")
  if n<2
    error("argument ''X'' must contain at least 2 entries per column")
  end
  sd=sqrt(sum((X-ones(n,1)*mean(X,"r")).^2,"r")/(n-1))
else
  error("argument ''cr'' must be ""c"" or ""r""")
end
endfunction