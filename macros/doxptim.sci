//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//----------------------------------------------------------------------------
function f=doxptimin(x,a0,a1,A)
//----------------------------------------------------------------------------
if or(abs(x)>1)
  f=%inf
else
  f=a0+x*(a1+A*x')
end
endfunction
//----------------------------------------------------------------------------
function f=doxptimax(x,a0,a1,A)
//----------------------------------------------------------------------------
if or(abs(x)>1)
  f=-%inf
else
  f=a0+x*(a1+A*x')
end
endfunction
//----------------------------------------------------------------------------
function [xopt,yopt]=doxptim(a,opt)
//----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if argin==1
  opt="max"
end
a=a(:)
p=length(a)
k1=(sqrt(8*(p-1)+1)-1)/2
k2=sqrt(2.25-2*(1-p))-1.5
if (k1>=1)&(k1==floor(k1))
  k=k1
  A=zeros(k,k)
elseif (k2>=1)&(k2==floor(k2))
  k=k2
  A=diag(a(k+2+k*(k-1)/2:$))
else
  error("argument ''a'' must be a vector of size 1+k(k+1)/2 or 1+k(k+3)/2")
end
a0=a(1)
a1=a(2:k+1)
u=k+2
for i=1:k-1
  for j=i+1:k
    A(i,j)=a(u)/2
    A(j,i)=a(u)/2
    u=u+1
  end
end
x0=zeros(1,k)
if opt=="min"
  [xopt,yopt]=torczon(x0,doxptimin,list(a0,a1,A),tol=1e-12,opt)
elseif opt=="max"
  [xopt,yopt]=torczon(x0,doxptimax,list(a0,a1,A),tol=1e-12,opt)
else
  error("argument ''opt'' must be ""min"" or ""max""")
end
endfunction