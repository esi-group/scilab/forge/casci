//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function x=idfoldednormal(y,mu,sigma,c)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>4)
  error("incorrect number of arguments")
end
if or((y<=0)|(y>=1))
  error("all elements of argument ''y'' must be in (0,1)")
end
if ~exists("mu","local")
  mu=0
end
if ~exists("sigma","local")
  sigma=1
end
if ~exists("c","local")
  c=0
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
x=c+sigma*sqrt(cdfchn("X",ones(y),ones(y)*(mu/sigma)^2,y,1-y))
endfunction