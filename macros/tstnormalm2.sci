//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=tstnormalm2(X,Y,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if argin==2
  t="~"
end
nx=size(X,"*")
ny=size(Y,"*")
mx=mean(X)
my=mean(Y)
sx=standev(X)
sy=standev(Y)
if t=="<"
  pv=cdfstudent((mx-my)/sqrt(sx^2/nx+sy^2/ny),nx+ny-2)
elseif t==">"
  pv=1-cdfstudent((mx-my)/sqrt(sx^2/nx+sy^2/ny),nx+ny-2)
elseif t=="~"
  pv=cdfstudent((mx-my)/sqrt(sx^2/nx+sy^2/ny),nx+ny-2)
  pv=2*min(pv,1-pv)
else
  error("argument ''t'' must be ""<"", "">"" or ""~""")
end
endfunction