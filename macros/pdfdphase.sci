//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfdphase(X,Q,q)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
q=q(:)'
r=1-sum(Q,"c")
Y=zeros(X)
[V,v]=spec(Q)
dv=diag(v)
iV=inv(V)
for i=find((X>=1)&(X==floor(X)))
  vr=diag(dv.^(X(i)-1))
  Y(i)=q*real(V*vr*iV)*r
end
Y(Y<0)=0
endfunction
