//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=doxpand(Z,ex)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if argin==1
  ex="x+xx"
end
[n,k]=size(Z)
X=[ones(n,1),Z]
if ex=="x"
  return
end
c=allcombination(2,1:k)
nc=size(c,"r")
Y=zeros(n,nc)
for i=1:nc
  Y(:,i)=Z(:,c(i,1)).*Z(:,c(i,2))
end
X=[X,Y]
if ex=="x+xx"
  return
end
Y=zeros(n,k)
for i=1:k
  Y(:,i)=Z(:,i).^2
end
X=[X,Y]
if ex=="x+xx+x2"
  return
else
  error("argument ''ex'' must be ""x"", ""x+xx"" or ""x+xx+x2""")
end
endfunction