//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function a=totalmedian(n)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
if or((n<=0)|(n~=floor(n)))
  error("argument ''n'' must be an integer >= 1")
end
nln=n*log(n)
A=zeros(n,n)
if modulo(n,2)==0
  m=n/2
  for i=1:n
    for k=0:m-1
      j=m-k+1:n-k
      A(i,i)=A(i,i)+exp(gammaln(n+1)-gammaln(k+1)-gammaln(n-k+1)-nln)*(i-1)^k*sum(combination((n-k)*ones(j),j).*((n-i).^(n-k-j)))
    end
  end
  for i=1:m
    for k=i+1:n-i+1
      A(i,k)=exp(gammaln(n+1)-nln-2*gammaln(m+1))*(i^m-(i-1)^m)*((n-k+1)^m-(n-k)^m)
      A(n-k+1,n-i+1)=A(i,k)
    end
  end
else
  m=(n+1)/2
  for i=1:n
    for k=0:m-1
      j=m-k:n-k
      A(i,i)=A(i,i)+exp(gammaln(n+1)-gammaln(k+1)-gammaln(n-k+1)-nln)*(i-1)^k*sum(combination((n-k)*ones(j),j).*((n-i).^(n-k-j)))
    end
  end
end
for i=1:m
  a(i)=(sum(A(i,i:n))+sum(A(1:i,i)))/2
  a(n-i+1)=a(i)
end
endfunction